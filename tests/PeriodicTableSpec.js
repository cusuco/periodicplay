
describe("Periodic Table", function () {
    let periodicPlay;
    const numElementsInPTable = 119;
    
    beforeEach(function () {
        periodicPlay = new PeriodicPlay();        
    });
    
    it("It should load all elements", function () {
        let elements = periodicPlay.loadData();

        expect(elements.length).toEqual(numElementsInPTable);
    });
    
    it("it should return seven periods", function () {
        let  ptPeriods = periodicPlay.getPeriods();
        expect(ptPeriods.length).toEqual(7);
    });

    it("it should retrieve the third period elements out of the periodic table", function () {
        let expectedNumOfElements = 8;
        let  period = periodicPlay.getPeriod(3);

        expect(period.length).toEqual(expectedNumOfElements);
    });

    it("it should retrieve a random number for a period in the periodic table", function () {
        let utilities = new Utilities();
        let randomPeriodNumber = utilities.getRandomIntInclusive(1,7);

        expect(randomPeriodNumber > 0).toBeTruthy();
        expect(randomPeriodNumber).toBeLessThan(8);
    });

    it("it should retrieve a random period out of the periodic table", function () {
        let utilities = new Utilities();
        let randomPeriodNumber = utilities.getRandomIntInclusive(1,7);
        let randomPeriod = periodicPlay.getPeriod(randomPeriodNumber);

        // now that we have the random period we need to make sure that every element
        //on the random period match the condition where their element period number property
        //is equal to the randomPeriodNumber
        let matchingElements = randomPeriod.filter(element => element.period === randomPeriodNumber);

        //now we check that our previous queried array has the same number of elements that
        // the random period obtain earlier
        expect(randomPeriod.length).toEqual(matchingElements.length);
    });

    it("it should retrieve a random element out of a random period", function () {
        //we get a random period fist
        let utilities = new Utilities();
        let randomPeriodNumber = utilities.getRandomIntInclusive(1,7);
        let randomPeriod = periodicPlay.getPeriod(randomPeriodNumber);

        //we produce a random position or index from where to retrieve the element
        let randomElementIndex = utilities.getRandomIntInclusive(0,randomPeriod.length-1);
        let elementAtRandomPosition = randomPeriod[randomElementIndex];

        expect(elementAtRandomPosition.period).toEqual(randomPeriodNumber);
    });

});