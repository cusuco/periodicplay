function ChemicalElement(name, symbol, atomicNumber, atomicMass) {
    this.name = name;
    this.symbol = symbol;
    this.atomicNumber = atomicNumber;
    this.atomicMass = atomicMass;
}


describe("Chemical Element", function () {
    it("should have an atomic mass", function () {
        let chemicalElement = new ChemicalElement('Helium','H',1,1.0079);
        expect(chemicalElement.atomicMass).toEqual(1.0079);
    });

    it("it should belong to a family", function () {
        
    });

    it("it should belong to a group", function () {

    });
});