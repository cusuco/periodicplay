/* UI Module supporting the creating of HTML Elements associate with the main app */

let PeriodicPlayUi = (function () {
    let utilities;
    let periodicTable;
    let currentChemicalElement;
    let allPeriods = []; //all periods of the periodic table.
    let processedChemicalElement = [];
    let periodsForCurrentGameLevel = []; //all the periods used for the current game level
    let numberOfChemicalElementsInGameLevel = 0;

    let periodicPlayUI = function () {
        utilities = new Utilities();
        periodicTable = new PeriodicTable();
        currentChemicalElement = {};
        allPeriods = periodicTable.getPeriods();
    };

    periodicPlayUI.prototype = {
        /**
         * it create a div element that contains a random chemical element
         * @param randomElement - the random element to be
         * @returns {HTMLElement}
         */
        createRandomElementDiv: function (randomElement) {
            currentChemicalElement = randomElement;

            /*add the random element name and symbol to the Div element*/
            let elementDiv = new Div().create(new Span().create(randomElement["symbol"]),
                new Paragraph().createParagraph(randomElement["name"]));

            elementDiv.id = randomElement.symbol;
            elementDiv.classList.add("chemical-element-small");
            elementDiv.setAttribute("draggable", "true");
            elementDiv.ondragstart = this.drag;
            return elementDiv;
        },

        /*it creates the div or container for the periods of the periodic table*/
        createPeriodsDiv: function () {
            let periodsContainer = new Div().create();
            let currentContext = this;
            periodicTable.getPeriods().forEach(function (chemicalElements, index) {
                let periodName = "Period " + (index + 1);
                let periodDiv = new Div().create();
                let spanElement = new Span().create(periodName);
                spanElement.classList.add("period-heading");

                periodDiv.id = periodName;
                periodDiv.classList.add("period-container");
                periodDiv.classList.add("flex-container");
                periodDiv.ondrop = this.drop(periodDiv);
                periodDiv.ondragover = this.allowDrop;

                chemicalElements.forEach(function (chemicalElement, index) {
                    let elementDiv = new Div().create(new Span().create(chemicalElement["number"]),
                        new Paragraph().createParagraph(chemicalElement["symbol"]));
                    elementDiv.classList.add("inline-chemical-element");
                    let elementCategory = chemicalElement["category"];
                    elementDiv.classList.add( elementCategory.replace(/ +/g, "-") );
                    periodDiv.appendChild(elementDiv);
                });
                periodsContainer.appendChild(spanElement);
                periodsContainer.appendChild(periodDiv);
            }, currentContext);

            periodsContainer.classList.add("periods-container");
            return periodsContainer;
        },

        createPeriodsRangeDiv: function(from, to) {
            let periodsContainer = new Div().create();
            let currentContext = this;

            for (let index = 0; index < to; index++)
            {
                let currentPeriod =  allPeriods[index];

                let periodName = "Period " + (index + 1);
                let periodDiv = new Div().create();
                let spanElement = new Span().create(periodName);
                spanElement.classList.add("period-heading");

                periodDiv.id = periodName;
                periodDiv.classList.add("period-container");
                periodDiv.classList.add("flex-container");
                periodDiv.ondrop = this.drop(periodDiv);
                periodDiv.ondragover = this.allowDrop;

                currentPeriod.forEach(function (chemicalElement, index) {
                    let elementDiv = new Div().create(new Span().create(chemicalElement["number"]),
                        new Paragraph().createParagraph(chemicalElement["symbol"]));
                    elementDiv.classList.add("inline-chemical-element");
                    let elementCategory = chemicalElement["category"];
                    elementDiv.classList.add(elementCategory.replace(/ +/g, "-"));
                    periodDiv.appendChild(elementDiv);

                    periodsContainer.appendChild(spanElement);
                    periodsContainer.appendChild(periodDiv);
                    numberOfChemicalElementsInGameLevel++;
                }, currentContext);
            }

            periodsContainer.classList.add("periods-container");
            return periodsContainer;
        },

        drag: function (evt) {
            evt.dataTransfer.setData("text", evt.target.id);
        },
        drop: function (periodDiv, context=this) {
            return function (event) {
                event.preventDefault();
                let data = event.dataTransfer.getData("text");
                let chemicalElementDiv = document.getElementById(data);

                /* we compare the period number of the element dragged against the period of the container.
                    if they match is counted as a successful attempt.
                */
                if (periodDiv.id.includes(currentChemicalElement.period) ) {
                    handleSuccessfulAttempt(periodDiv, chemicalElementDiv);
                    if(processedChemicalElement.length === numberOfChemicalElementsInGameLevel) {
                        handleGameWon();
                        return;
                    }
                    doAfterSuccessfulAttempt(context);
                } else {
                    handleUnsuccessfulAttempt();
                }
            };
        },
        allowDrop: function (event) {
            event.preventDefault();
        }
    };

    /* It handles the successful attempt of dropping the right chemical element */
    function handleSuccessfulAttempt(periodDiv, chemicalElementDiv) {
        /* remove previous unsuccessful attempts */
        document.body.classList.remove("unsuccessfulAttempt");
        /* remove previous successful attempts */
        document.body.classList.remove("successfulAttempt");
        /* look for the element and turn it on */
        for (let i = 0; i < periodDiv.childElementCount; i++) {
            let element = periodDiv.children[i];
            let p = element.getElementsByTagName("p");

            if (p[0].innerHTML === currentChemicalElement.symbol) {
                element.style.visibility = "visible";
            }
        }
        chemicalElementDiv.style.display = "inline-block";

        //we need to hide the label to make space for the chemical element being dragged onto
        periodDiv.firstElementChild.classList.add("hidden-label");
        periodDiv.appendChild(chemicalElementDiv);
        periodDiv.removeChild(chemicalElementDiv);


        setTimeout(() => {
            document.body.classList.add("successfulAttempt");
        }, 200);

        processedChemicalElement.push(currentChemicalElement);
        currentChemicalElement = null;
    }

    function doAfterSuccessfulAttempt(context) {
        let periodicPlayMain = new PeriodicPlayMain();
        // we need to look up the game level requested by the user */
        let randomElement = periodicPlayMain.createRandomElement(periodicPlay_data.currentGameLevel.from,
            periodicPlay_data.currentGameLevel.to);

        /* we can't accept a random element that already has been dropped successfully */
        //keep getting a random element div until find one that hasn't been dropped successfully
        let previousChemicalElement = processedChemicalElement.find(e => e.symbol ===  randomElement.symbol);

        while(previousChemicalElement != undefined){
            randomElement = periodicPlayMain.createRandomElement(periodicPlay_data.currentGameLevel.from,
                periodicPlay_data.currentGameLevel.to);
            previousChemicalElement = processedChemicalElement.find(e => e.symbol ===  randomElement.symbol);
        }

        let randomDiv = context.createRandomElementDiv(randomElement);
        document.getElementById("mainDiv").appendChild(randomDiv);
    }

    function handleGameWon() {

            document.body.classList.remove("successfulAttempt");
            setTimeout(() => {
                document.body.classList.add("successfulAttempt");
            }, 200);
            return;

    }

    function handleUnsuccessfulAttempt() {
        document.body.classList.remove("successfulAttempt");
        document.body.classList.remove("unsuccessfulAttempt");
        setTimeout(() => {
            document.body.classList.add("unsuccessfulAttempt");
        }, 200);
    }
    return periodicPlayUI;
})();

