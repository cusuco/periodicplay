/**
 * This application module is responsible for the main logic
 * @type {PeriodicPlayMain}
 */
let PeriodicPlayMain = (function () {
    let periodicPlay;
    let utilities;
    let periodicPlayUI;

    let periodicPlayMain = function () {
        periodicPlay = new PeriodicTable();
        utilities = new Utilities();
        periodicPlayUI = new PeriodicPlayUi();
    };

    periodicPlayMain.prototype = {
        /**
         * it creates a random chemical element from Periods that are included in the
         * "from to" range.
         * @param from the first period number
         * @param to the last period number
         * @returns {*}
         */
        createRandomElement : function(from, to){
            let randomPeriodNumber = utilities.getRandomIntInclusive(from,to);
            let randomPeriod = periodicPlay.getPeriod(randomPeriodNumber);

            //we produce a random position or index from where to retrieve the element
            let randomElementIndex = utilities.getRandomIntInclusive(0,randomPeriod.length-1);
            return randomPeriod[randomElementIndex];
        },

        handleChoosingGameLevel: function(from, to){

            let randomElement = this.createRandomElement(from, to);
            let randomChemicalElement = periodicPlayUI.createRandomElementDiv(randomElement);
            let periodsDiv = periodicPlayUI.createPeriodsRangeDiv(from, to);

            return {
                'periodsDiv' : periodsDiv,
                'randomChemicalElementDiv' : randomChemicalElement
            };
        }
    };
    return periodicPlayMain;
})();