function Heading() {
    
}
Heading.prototype.create = function (headingTag, headingText) {
    let headingElement = document.createElement(headingTag);
    
    if (typeof (headingText) !== 'undefined') {
        var textNode = document.createTextNode(headingText);
        headingElement.appendChild(textNode);
    }
    return headingElement;
};