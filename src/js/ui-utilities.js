function Heading() {

}
Heading.prototype.create = function (headingTag, headingText) {
    let headingElement = document.createElement(headingTag);

    if (typeof (headingText) !== 'undefined') {
        var textNode = document.createTextNode(headingText);
        headingElement.appendChild(textNode);
    }
    return headingElement;
};

function Paragraph() {

}

/*
    it creates a paragraph with the given text.
    @param: the text that goes inside the paragraph
    @return: a paragraph HTML element
 */
Paragraph.prototype.createParagraph = function (text) {
    console.log("creating a paragraph");

    var paragraphElement = document.createElement("P");

    if (typeof (text) !== 'undefined') {
        var textNode = document.createTextNode(text);
        paragraphElement.appendChild(textNode);
    }

    return paragraphElement;
};

function Div() {
}

/*
    it creates a DIV with the given text.
    @param: the text that goes inside the paragraph
    @return: a DIV HTML element
 */

Div.prototype.create = function (...otherElements) {
    let utilities = new Utilities();
    console.log(otherElements);

    let mainElement = document.createElement("DIV");
    // now iterate over otherElements (which is an array)
    for (let subElement of otherElements) {
        // check what type of subElement we have
        if (subElement instanceof HTMLElement)
        // add any HTMLElement
            mainElement.appendChild(subElement);
        else if (utilities.isString(subElement)) {
            // we have a String, so create a TextNode
            let textNode = document.createTextNode(subElement);
            mainElement.appendChild(textNode);
        } else
            console.log("ERROR: createDiv() failed with subElement: " + subElement);
    }
// return the mainElement so the caller can use it
    return mainElement;
};

function Span() {
}

Span.prototype.create = function (text) {
    let spanElement = document.createElement("Span");

    if (typeof (text) !== 'undefined') {
        var textNode = document.createTextNode(text);
        spanElement.appendChild(textNode);
    }
    return spanElement;
};

function Label() {

}

Label.prototype.create = function (text) {
    var labelElement = document.createElement("LABEL");

    if (typeof (text) !== 'undefined') {
        var textNode = document.createTextNode(text);
        labelElement.appendChild(textNode);
    }
    return labelElement;
};

function Button() {
}

Button.prototype.create = function (btnText) {
    var btnElement =  document.createElement("BUTTON");

    //handles adding the text for the button
    if (typeof (btnText) !== 'undefined') {
        var textNode = document.createTextNode(btnText);
        btnElement.appendChild(textNode);
    }
    
    return btnElement;    
};

function TextInput() {
}

TextInput.prototype.create = function () {
    let textInputElement = document.createElement("INPUT");
    textInputElement.setAttribute("type", "text");
    return textInputElement;
};

function Image() {
}

Image.prototype.create = function () {
    return document.createElement("IMG");
};

/*
    Complex UI consisting of a Label, Button and Input
 */
function LabelledTextField() {
}

/*
    it creates an Object containing a Label, Button and Input
    component.
    @labelText - a text that will be displayed on the label
    @btnText - a text that will be displayed on the button
 */
LabelledTextField.prototype.create = function (labelText, btnText) {
    
    var labelElement = new Label().create();
    var buttonElement = new Button().create();
    
    //handles adding the text for the label
    if (typeof (labelText) !== 'undefined') {
        let textNode = document.createTextNode(labelText);
        labelElement.appendChild(textNode);
    }

    //handles adding the text for the label
    if (typeof (btnText) !== 'undefined') {
        let textNode = document.createTextNode(btnText);
        buttonElement.appendChild(textNode);
    }
    
    var textInputElement = new TextInput().create();

    return {
        label: labelElement,
        input: textInputElement,
        button: buttonElement
    };
};

function RadioInputWithLabel(){

}

/**
 *
 * @param labelText the text label that will be placed next to the radio input
 * @param radioInputTypeOptions
 * @returns {HTMLElement}
 */
RadioInputWithLabel.prototype.create = function(labelText,radioInputTypeOptions){
    let labelElement = new Label().create(labelText);
    let radioInput = document.createElement("INPUT");

    radioInput.setAttribute("name",radioInputTypeOptions.name);
    radioInput.setAttribute("value",labelText);
    radioInput.setAttribute("type","radio");
    radioInput.onclick = radioInputTypeOptions.onclickFunction;

    return new Div().create(labelElement,radioInput);
};









