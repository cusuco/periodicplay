/*This module is responsible for responding to the user interaction with the menus*/

let PeriodicPlayMenu = (function () {
    let currentChosenGameLevel;
    new PeriodicPlayUi();
    let periodicPlayMain = new PeriodicPlayMain();
    let periodicTableLookup = new PeriodicTable();
    let periods = [];

    let periodicPlayMenu = function () {
        periods = periodicTableLookup.getPeriods();
    };

    periodicPlayMenu.prototype = {
        /**
         *  it creates the start window/screen for the game
         */
        createStartScreen: function () {
            let settingsBtn = new Button().create("Game Settings");
            settingsBtn.classList.add("game-btn");
            settingsBtn.onclick = this.loadSettings;

            let menuDiv = new Div().create(settingsBtn);
            menuDiv.classList.add("btn-flex-container");
            return menuDiv;
        },

        /**
         *  It loads the App Settings UI  components into the HTML DOC
         */
        loadSettings: function () {
            // we need to clear previous screen UI elements
            removeAllChildren(document.body);

            let radioInputTypeOptions = {
                name: "game_level",
                onclickFunction: onGameLevelClick //the function that will be invoke on the 'onclick' event of
                                                   //radio box
            };

            let radioInputComponent = new RadioInputWithLabel().create("Level 1", radioInputTypeOptions);
            let radioInputLevel2 = new RadioInputWithLabel().create("Level 2", radioInputTypeOptions);
            let radioInputLevel3 = new RadioInputWithLabel().create("Level 3", radioInputTypeOptions);
            let radioInputLevel4 = new RadioInputWithLabel().create("Level 4", radioInputTypeOptions);

            /*create the button for submitting the game level*/
            let submitButton = new Button().create("Submit");
            submitButton.onclick = submitGameLevel;

            let gameSettingsDiv = new Div().create(radioInputComponent, radioInputLevel2,
                radioInputLevel3, radioInputLevel4, submitButton);
            gameSettingsDiv.classList.add("game-settings");

            let headerElement = new Heading().create("h4", "Please select a game level");
            let mainDiv = new Div().create(headerElement, gameSettingsDiv);
            mainDiv.classList.add("main-div-game-settings");
            document.body.appendChild(mainDiv);
        },

        loadApp: function () {
            // we need to clear previous game sessions
            removeAllChildren(document.body);

            let periodicPlayMain = new PeriodicPlayMain();
            let randomElement = periodicPlayMain.createRandomElement(1, 7);

            let periodicPlayUI = new PeriodicPlayUi();
            let randomChemicalElement = periodicPlayUI.createRandomElementDiv(randomElement);

            /* set up the main div */
            let mainDiv = new Div().create();
            mainDiv.id = "mainDiv";
            document.body.appendChild(mainDiv);

            /* create div container for periods of the periodic table */
            let periodsDiv = periodicPlayUI.createPeriodsDiv();

            mainDiv.appendChild(periodsDiv);
            mainDiv.appendChild(randomChemicalElement);
        }
    };

    /**
     * It handles the event triggered when a user submit a game level
     * @param event
     */
    function submitGameLevel(event) {
        /* set up the main div */
        let result;
        let mainDiv = new Div().create();
        mainDiv.id = "mainDiv";
        document.body.appendChild(mainDiv);

        removeAllChildren(document.body);

        switch (currentChosenGameLevel) {
            case "Level 1":
                result = periodicPlayMain.handleChoosingGameLevel(1, 3);
                periodicPlay_data.currentGameLevel = {"from" : 1 , "to": 3};
                break;
            case "Level 2":
                result = periodicPlayMain.handleChoosingGameLevel(1, 5);
                periodicPlay_data.currentGameLevel = {"from" : 1 , "to": 5};
                break;
            case "Level 3":
                result = periodicPlayMain.handleChoosingGameLevel(1, 7);
                periodicPlay_data.currentGameLevel = {"from" : 1 , "to": 7};
               break;
            case "Level 4":
                result = periodicPlayMain.handleChoosingGameLevel(1, 9);
                periodicPlay_data.currentGameLevel = {"from" : 1 , "to": 9};
                break;
            default:
                console.log("default reached");
        }
        mainDiv.appendChild(result["periodsDiv"]);
        mainDiv.appendChild(result["randomChemicalElementDiv"]);
    }

    /**
     * it handles the onclick event generated when a user click on a Game Level radio button
     * @param event
     */
    function onGameLevelClick(event) {
        currentChosenGameLevel = event.target.value;
    }

    /**
     *  it removes children nodes from a parent element
     * @param parentElement
     */
    function removeAllChildren(parentElement) {
        let numChildren = parentElement.childElementCount;
        for (let index = 0; index < numChildren; index++) {
            let childElement = parentElement.children[index];

            if (childElement) {
                parentElement.removeChild(childElement);
            }
        }
    }
    return periodicPlayMenu;
})();
