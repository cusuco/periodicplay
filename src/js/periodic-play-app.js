window.onload = function () {

    let periodicPlayMenu = new PeriodicPlayMenu();
    let appMenuDiv =  periodicPlayMenu.createStartScreen();
    document.body.appendChild(appMenuDiv);
};

/**
 *  it removes children nodes from a parent element
 * @param parentElement
 */
function removeAllChildren(parentElement){
    let numChildren = parentElement.childElementCount;
    for (let index = 0; index < numChildren; index++){
        let childrenElement = parentElement.children[index];
        parentElement.removeChild(childrenElement);
    }
}







