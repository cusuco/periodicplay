let Utilities = (function () {

    let Utilities = function () {
    };

    Utilities.prototype = {

        /**
         * it sets the body of a HTML Document to a random colour from a range of colours list
         * that are stored in a javascript structure/array.
         * This array is available via the 'data' namespace (i.e data.colourList)
         *
         */
        randomiseBackgroundColour: function () {
            let randomColour = this.getRandomIntInclusive(0, data.colourList.length - 1);
            document.body.style.backgroundColor = data.colourList[randomColour];
        },

        /**
         * Returns a random integer between min (inclusive) and max (inclusive).
         * The value is no lower than min (or the next integer greater than min
         * if min isn't an integer) and no greater than max (or the next integer
         * lower than max if max isn't an integer).
         * Using Math.round() will give you a non-uniform distribution!
         *
         * source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
         */
        getRandomIntInclusive: function (min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },

        /**
        This is a utility function to tell if a given parameter is a String or not.
        Returns: true/false
         From SIT708 Theory and Exercise booklet
        */
        isString : function (s) {
            return typeof (s) === 'string' || s instanceof String;
        }
    };

    return Utilities;

})();